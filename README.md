# IBM CICS Server access with OO in VB

From the depths of the wonderful world of technology at the turn of the millennium, object-oriented code implemented in Visual Basic to accomplish the difficult task of communication between the mainframe and the internet world.

---

OO CICS Client Visual Basic Sample Programs for Windows 98 and Windows NT
=========================================================================

*******************************************************************************
*                                ***WARNING***                                *
*                                                                             *
* All references to the "CICSCLI" directory in these sample files must be     *
* replaced by the destination directory that the product was installed into,  *
* in order to build the programs.                                             *
*                                                                             *
*******************************************************************************

The following sample programs are provided to demonstrate the OO CICS Client
OLE interfaces. These samples are located in directory CICSCLI\SAMPLES\VB.

The samples use the constants provided in the OO CICS Client Type Libraries
CCLOECI.TLB and CCLOEPI.TLB. These files are located in directory CICSCLI\OLE.


ECIVB1

  Description:- This sample program demonstrates the use of the CICS ECI
      OLE interfaces to list the CICS servers known to the client, make
      link calls to the server program "ECIWTO", and to either commit or
      backout a CICS unit of work.

  Notes:- This sample requires that the server program ECIWTO, located
      in CICSCLI\SAMPLES\SERVER is built and installed on the CICS server.
      On CICS/6000 servers EXEC CICS WRITE OPERATOR is not supported,
      so this part of the program should be removed or replaced with code
      to write the text string to a TS queue or a file.


ECIVB2

  Description:- This sample program demonstrates several functions of the
      Ccl.Buffer interface. It does not require a CICS server program.


EPIVB1

  Description:- This sample program demonstrates the use of the CICS EPI
      OLE interfaces to run a CICS server application via its BMS/3270
      user interface, including the use of BMS map symbolic names in
      the client application.

  Notes: This sample requires that transaction 'EPIC' and its program
      EPIINQ are built and installed on the CICS server.

